"""
Convert dictionary values back and forth between Applescript and Python.

The Associative Array is a custom data type inside the ASTranslate/PYTranslate
modules. On the Python side, it is simply a delimited string that Python can
create from a dictionary or parse into one.
"""

from . import PBlist


class invalidArrayString(Exception):
	pass


def to_applescript(pyDict):
	"""
	Convert a dictionary into an Associative Array string.

	This string is used to print out to applescript."""
	pairs = []
	for k, v in list(pyDict.items()):
		if isinstance(v, list):
			v = PBlist.to_applescript(v)
		pairs.append("<" + k + "=" + v + ">")

	return "".join(pairs)


def to_python(asArray):
	"""
	Convert an Associative Array passed in from AppleScript into a dictionary.
	"""
	if (len(asArray) < 3) or ("<" not in asArray) or (">" not in asArray):
		raise invalidArrayString("'" + str(asArray) + "' is not a valid Array")

	dict = {}
	asArray = str(asArray[1:len(asArray) - 1])
	pairs = asArray.split("><")
	for pair in pairs:
		k, v = pair.split("=")
		dict[k] = v
	return dict
