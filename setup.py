import setuptools

with open("README.md", "r") as readme:
	long_description = readme.read()

setuptools.setup(
	name="pastybridge",
	version="1.1.1",
	url="https://github.com/darricktheprogrammer/pastybridge",

	author="Darrick Herwehe",
	author_email="darrick@exitcodeone.com",

	description="An Applescript/Python Translation layer",
	long_description=long_description,
	license="MIT",

	packages=setuptools.find_packages(),

	install_requires=[],

	classifiers=[
		"Development Status :: 5 - Production/Stable",
		"Programming Language :: Python",
		"Programming Language :: Python :: 3",
		"Programming Language :: Python :: 3.9",
	],
)
