PAsty-Bridge
============

A collection of functions for converting values back and forth between Applescript and Python.

Contents include:

1. A single applescript file to take text from `do shell script` commands and convert the output into native applescript types.
2. A python package for taking arguments and converting them into native Python types.

This repo is only lightly maintained when necessary. The most recent changes were to update from Python 2.7 to 3.x.


## Simple examples

```applescript
use pyLib : script "pytranslate"

-- Send a list to the external script
set ls to {"opt1", "opt2", "opt3"}
set ls_str to pyLib's list_to_python(ls)
set script_result to do shell script quoted form of ("python3 /path/to/script.py " & ls_str)

-- Convert the result back from text to native format
set calculated_date to pyLib's date_to_applescript(script_result)
```

```python
from argparse import ArgumentParser

from astranslate import PBdate, PBlist

from . import calculate_date_from_options  # function from your own package


def main(args):
	option_list = PBlist.list_to_python(args.date_options)
	correct_date = calculate_date_from_options(option_list)
	print(PBdate.to_applescript(correct_date))


if __name__ == "__main__":
	parser = ArgumentParser(description=__doc__)
	parser.add_argument(
		'date_options',
		type=str,
		help='A list of options to calculate a date',
	)
	main(parser.parseargs())
```


## Available functions

This is just a list of functions and should be somewhat self-explanatory. There is more detailed descriptions in the code files.

**Python:**

* `astranslate.PBbool.to_applescript()`
* `astranslate.PBbool.to_python()`
* `astranslate.PBdate.to_applescript()`
* `astranslate.PBdate.to_python()`
* `astranslate.PBdict.to_applescript()`
* `astranslate.PBdict.to_python()`
* `astranslate.PBlist.to_applescript()`
* `astranslate.PBlist.to_python()`
* `astranslate.PBnumber.to_applescript()`
* `astranslate.PBnumber.to_python()`
* `astranslate.PBstring.to_applescript()`
* `astranslate.PBstring.to_python()`


**Applescript:**

* `number_to_applescript()`
* `number_to_python()`
* `bool_to_applescript()`
* `bool_to_python()`
* `list_to_applescript()`
* `list_to_python()`
* `date_to_applescript()`
* `date_to_python()`
* `string_to_applescript()`
* `string_to_python()`
* `array_to_applescript()`
* `array_to_python()`
