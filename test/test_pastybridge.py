#!/usr/bin/env python3
from datetime import datetime

import pytest

from astranslate import PBbool, PBdate, PBdict, PBlist, PBnumber, PBstring


def test_PBBool_ToApplescript_GivenBoolean_ReturnsBooleanString():
	assert PBbool.to_applescript(True) == "True"


def test_PBBool_ToApplescript_GivenNumber_ReturnsNumberAsString():
	assert PBbool.to_applescript(1) == "1"


def test_PBBool_ToPython_GivenTrue_ReturnsBoolean():
	assert PBbool.to_python("true") is True


def test_PBBool_ToPython_GivenYes_ReturnsBoolean():
	assert PBbool.to_python("yes") is True


def test_PBBool_ToPython_Given1_ReturnsBoolean():
	assert PBbool.to_python("1") is True


def test_PBDate_ToPython_GivenExpectedDateString_ReturnsDateObject():
	date_obj = PBdate.to_python("Thursday, January 1, 1970 12:00:00 AM")
	assert isinstance(date_obj, datetime)
	assert date_obj.year == 1970
	assert date_obj.month == 1
	assert date_obj.day == 1
	assert date_obj.hour == 0
	assert date_obj.minute == 0
	assert date_obj.second == 0


def test_PBDate_ToApplescript_GivenDatetimeObject_ReturnsExpectedString():
	date_obj = datetime(1970, 1, 1, 0, 0, 0)
	date_str = PBdate.to_applescript(date_obj)
	assert date_str == "Thursday, January 01, 1970 12:00:00 AM"


def test_PBDict_ToApplescript_GivenDictWithList_ReturnsExpectedString():
	test_dict = {
		"key1": "value1",
		"key2": "value2",
		"key3": ["list", "of", "values"],
	}
	dict_str = PBdict.to_applescript(test_dict)
	assert dict_str == "<key1=value1><key2=value2><key3={list|of|values}>"


def test_PBDict_ToPython_GivenBasicDict_ReturnsExpectedDict():
	dict_str = PBdict.to_python("<key1=value1><key2=value2>")
	assert len(dict_str) == 2
	assert list(dict_str) == ["key1", "key2"]


def test_PBList_ToApplescript_GivenBasicList_ReturnsExpectedString():
	ls_str = PBlist.to_applescript(["a", "b", "c", "d", "e"])
	assert ls_str == "{a|b|c|d|e}"


# @pytest.mark.xfail
def test_PBList_ToApplescript_GivenNestedList_ReturnsExpectedString():
	ls_str = PBlist.to_applescript(["a", "b", "c", ["d", "e"]])
	assert ls_str == "{a|b|c|{d|e}}"


def test_PBList_ToPython_GivenBasicList_ReturnsExpectedList():
	ls_str = PBlist.to_python("{a|b|c|d|e}")
	assert isinstance(ls_str, list)
	assert ls_str == ["a", "b", "c", "d", "e"]


@pytest.mark.xfail
def test_PBList_ToPython_GivenNestedList_ReturnsExpectedList():
	ls_str = PBlist.to_python("{a|b|c|{d|e}}")
	assert ls_str == ["a", "b", "c", ["d", "e"]]


def test_PBNumber_ToApplescript_GivenInteger_ReturnsNumberString():
	assert PBnumber.to_applescript(2) == "2"


def test_PBNumber_ToApplescript_GivenFloat_ReturnsNumberString():
	assert PBnumber.to_applescript(3.9) == "3.9"


def test_PBNumber_ToPython_GivenIntString_ReturnsInt():
	assert PBnumber.to_python("3") == 3


def test_PBNumber_ToPython_GivenFloatString_ReturnsFloat():
	assert PBnumber.to_python("3.9") == 3.9


def test_PBString_ToApplescript_GivenString_ReturnsSameString():
	assert PBstring.to_applescript("a string of text") == "a string of text"


def test_PBString_ToPython_GivenString_ReturnsSameString():
	assert PBstring.to_python("a string of text") == "a string of text"
